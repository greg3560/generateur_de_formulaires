// on s'assure que le DOM est entierement defini
$(function()
{
	// ajout de l'evenement click sur le bouton label
	$('#label').click(function() {
		/* Insertion d'ne div d'edition que l'on pourra facilement suprimer.
		Cette div contient un label, un input et un bouton
		La partie bottom du panneau droit.*/
		$('hr').after('<div id="edit"><label for="text">Texte du label</label><br /><input type="text" id="text" name="text" /><button id="lblok">OK</button><button id="lblAnnuler">Annuler</button></div>');
		$('#edit').hide().fadeIn(1000);
		$('#text').focus();// on donne le focus sur le champ qui vient d'etre crée
		$('#label, #zdt, #bouton').attr('disabled', 'disabled');// desactivation des boutons label zone de texte et bouton

		// ajout de l'evenement click sur le bouton OK
		$('#lblok').click(function() {
			var req = '<span>' + $('#text').val() + '</span>';// insertion d'un span contenant la valeur de l'input text
			$('#gauche').append(req);// insertion a la fin de la selection. La div gauche.
			$('#edit').fadeOut(1000, function() {$(this).remove();});// suppression de la div d'edition avec un fadeout 1 sec.
			$('#label, #zdt, #bouton').removeAttr('disabled'); // activation des boutons label zone de texte et bouton
		});

		// gestion du bouton annuler avec la disparition progreesive de la div edit en 1 sec et suppression de l'attribut disabled
		$('#lblAnnuler').click(function() {
			$('#edit').fadeOut(1000, function() {$(this).remove();});
			$('#label, #zdt, #bouton').removeAttr('disabled');// activation des boutons label zone de texte et bouton
		});

		// animation de l'ombre sur le bouton ok au survol de la souris
		// debut du survol
		$('#lblok').mouseover(function() {
			$(this).css('boxShadow', '5px 5px 6px #7A7A7A');
		});
		// fin du survol
		$('#lblok').mouseout(function() {
			$(this).css('boxShadow', 'none');
		});
	});
	// Pour le bouton zone de texte c'est la meme chose sauf qu'ici on insere un champ input text avec pour id la valeur de la zone de texte #textzdt
	$('#zdt').click(function() {
		$('hr').after('<div id="edit"><label for="textzdt">id de la zone de texte</label><br /><input type="text" id="textzdt" name="textzdt" /><button id="zdtok">OK</button><button id="zdtAnnuler">Annuler</button></div>');
		$('#edit').hide().fadeIn(1000);
		$('#textzdt').focus();
		$('#label, #zdt, #bouton').attr('disabled', 'disabled');

		$('#zdtok').click(function() {
			var req = '<input type="text" id="' + $('#textzdt').val() + '" /><br />';
			$('#gauche').append(req);
			$('#edit').fadeOut(1000, function() {$(this).remove();});
			$('#label, #zdt, #bouton').removeAttr('disabled');
		});
		$('#zdtAnnuler').click(function() {
			$('#edit').fadeOut(1000, function() {$(this).remove();});
			$('#label, #zdt, #bouton').removeAttr('disabled');
		});
		$('#zdtok').mouseover(function() {
			$(this).css('boxShadow', '5px 5px 6px #7A7A7A');
		});
		$('#zdtok').mouseout(function() {
			$(this).css('boxShadow', 'none');
		});
	});
	// pour le bouton c'est identique au bouton label. Il y a juste les balise <button>
	$('#bouton').click(function() {
		$('hr').after('<div id="edit"><label for="textbtn">Texte du bouton</label><br /><input type="text" id="textbtn" name="textbtn" /><button id="btnok">OK</button><button id="btnAnnuler">Annuler</button></div>');
		$('#edit').hide().fadeIn(1000);
		$('#textbtn').focus();
		$('#label, #zdt, #bouton').attr('disabled', 'disabled');

		$('#btnok').click(function() {
			var req = '<button>' + $('#textbtn').val() + '</button>';
			$('#gauche').append(req);
			$('#edit').fadeOut(1000, function() {$(this).remove();});
			$('#label, #zdt, #bouton').removeAttr('disabled');
		});
		$('#btnAnnuler').click(function() {
			$('#edit').fadeOut(1000, function() {$(this).remove();});
			$('#label, #zdt, #bouton').removeAttr('disabled');
		});
		$('#btnok').mouseover(function() {
			$(this).css('boxShadow', '5px 5px 6px #7A7A7A');
		});
		$('#btnok').mouseout(function() {
			$(this).css('boxShadow', 'none');
		});
	});
});

